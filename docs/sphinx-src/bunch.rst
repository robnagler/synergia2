bunch module
==================
The bunch module contains the Bunch class and related classes.

---------
Classes
---------

.. doxygenclass:: Bunch
   :project: synergia

.. doxygenfunction:: populate_6d
   :project: synergia

.. doxygenfunction:: populate_transverse_gaussian
   :project: synergia

.. doxygenfunction:: populate_uniform_cylinder
   :project: synergia

.. doxygenclass:: Fixed_t_z_converter
    :project: synergia

.. doxygenclass:: Fixed_t_z_zeroth
    :project: synergia

.. doxygenclass:: Fixed_t_z_ballistic
    :project: synergia

.. doxygenclass:: Diagnostics
    :project: synergia

.. doxygenclass:: Diagnostics_full2
    :project: synergia

.. doxygenclass:: Diagnostics_particles
    :project: synergia

.. doxygenclass:: Diagnostics_track
    :project: synergia

.. doxygenclass:: Multi_diagnostics
    :project: synergia

