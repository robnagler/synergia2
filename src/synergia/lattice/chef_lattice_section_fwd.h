#ifndef CHEF_LATTICE_SECTION_FWD_H_
#define CHEF_LATTICE_SECTION_FWD_H_

#include <boost/shared_ptr.hpp>

class Chef_lattice_section;

typedef boost::shared_ptr<Chef_lattice_section > Chef_lattice_section_sptr;

#endif /* CHEF_LATTICE_SECTION_FWD_H_ */
